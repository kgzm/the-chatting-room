(ns chat.state
  (:require [reagent.core :as reagent :refer [atom]])
  )
(defonce messages (atom []))
(defonce state (atom {}))

(defn update-state [new-state]
  (swap! state merge new-state))

(defn current-room []
  (:room @state))

(defn all-rooms []
  (:rooms @state))

(defn append-message [message]
  (swap! messages conj message)
  (.log js/console (pr-str @messages)))
