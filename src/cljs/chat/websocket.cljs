(ns chat.websocket
  (:require [chat.state :refer [append-message update-state]]
            [cljs.reader :refer [read-string]]))

(defonce websocket (atom nil))

(defn send-socket [data]
  (when @websocket (.send @websocket (pr-str data))))
(def socket-url (str "ws://" 
                     (-> js/window .-location .-host) 
                     "/chat"))
(defn open-socket []
  (if (or (nil? @websocket)
          (= (.-readyState @websocket) (.-CLOSING @websocket))
          (= (.-readyState @websocket) (.-CLOSED @websocket)))
    (do (reset! websocket (js/WebSocket. socket-url ))
        (aset @websocket "onmessage" 
              (fn [e]
                (.log js/console "Got something for ye.")
                (when-let [message  (read-string (.-data e))]
                  (.log js/console (pr-str message))
                  (condp = (:type message)
                    :state (update-state (:state message))
                    (append-message message)
                    )))))))

(open-socket)
