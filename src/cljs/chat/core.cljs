(ns chat.core
    (:require [reagent.core :as reagent :refer [atom]]
              [reagent.session :as session]
              [secretary.core :as secretary :include-macros true]
              [goog.events :as events]
              [cljs.core.async :as async :refer [>! <! chan]]
              [cljs.reader :refer [read-string]]
              [goog.history.EventType :as EventType]
              [chat.websocket :refer [open-socket send-socket]]
              [chat.state :refer [messages current-room all-rooms]]
              )
    (:require-macros [cljs.core.async.macros :as m :refer [go go-loop]])
    (:import goog.History))

;;Websockets..
;; -------------------------
;; Views
(defn chat-input []
  (let [input (atom "")]
    (fn []
      [:div
       [:textarea#input
        {:value @input
         :on-change #(reset! input (-> % .-target .-value))
         :onKeyUp #(when (= 13 (-> % .-keyCode))
                     (send-socket {:type :talk
                                   :message @input})
                     (reset! input ""))}]])))
(defn show-message [message]
  [:div.message 
   {:class (str "message-"(name (:type message)))}
   (:message message)]
  )

(defn should-show-message? [message]
  (or (= (:room message) (current-room))
      (= (:type message) :system)))

(defn list-messages []
  (mapv show-message (filter should-show-message? @messages )))

(defn home-page [] 
  [:div#main [:h1 "The Chatting Room"]
   [:div#main
    (into [:div#buffer] (list-messages))]
    (into [:div#rooms] (all-rooms))
   [chat-input]
   ])



(defn current-page []
  [:div [(session/get :current-page)]])

;; -------------------------
;; Routes
(secretary/set-config! :prefix "#")

(secretary/defroute "/" []
  (session/put! :current-page #'home-page))


;; -------------------------
;; History
;; must be called after routes have been defined
(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
     EventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

;; -------------------------
;; Initialize app
(defn mount-root []
  (reagent/render [current-page] (.getElementById js/document "app")))

(defn init! []
  (hook-browser-navigation!)
  (mount-root))
