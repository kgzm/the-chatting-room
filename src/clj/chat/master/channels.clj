(ns chat.master.channels
  (:require [clojure.core.async :refer [chan mix pub]]
            [chat.log :refer [log]] 
            ))

(defn topic [message]
  (apply str 
         (filterv (complement nil?) 
                  [(:type message)
                   (:room message)])))

(defonce input-channel (chan))
(defonce input-mix (mix input-channel))
(defonce output-channel (chan))
(defonce output-publisher (pub output-channel topic))
