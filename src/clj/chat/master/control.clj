(ns chat.master.control
  (:require [chat.connections :refer [take-conn drop-conn]]
            [chat.actions :refer [dispatch]]
            [chat.master.channels :refer :all]
            [clojure.core.async :refer [go-loop <! admix]]
            [chat.log :refer [log]]))

(defonce run-id (atom 0))
(defn run []
  (let [this-run-id (swap! run-id inc)]
    (go-loop 
      []
      (when (= this-run-id @run-id)
        (when-let [msg (<! input-channel)]
          (log "Received a message." msg)
          (condp = (:type msg)
            :opening (admix input-mix (take-conn msg)) 
            :data (dispatch msg)
            :closing (drop-conn msg)
            nil) 
          (recur))))))

(run)
