(ns chat.master.state)

(defonce input-channel (chan))
(defonce input-mix (mix input-channel))
(defonce output-channel (chan))
(defonce output-publisher (pub output-channel #(:room %)))
