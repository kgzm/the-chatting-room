(ns chat.connections
  (:require [chat.state :refer [app-state]]
            [chat.master.channels :as mchans]
            [clojure.core.async :refer [chan mult mix admix tap]]
            [chat.log :refer :all]
            [chat.user :as user]
            ))


(defn take-conn
  "Accepts map of session key, input channel , and output channel.
  Mixes existing inputs + supplied inputs down to a single input channel.
  Multiplexes a single output channel to existing outputs + supplied output.

  Returns input channel."
  [{:keys [session] :as conn}]
  (let [user (get-in @app-state [:sessions session])
        output       (or (:output user) (chan)) 
        output-mult  (or (:output-mult user) (mult output))
        input        (or (:input user) (chan))
        input-mix    (or (:input-mix user) (mix input))]
    (admix input-mix (:input conn))
    (tap output-mult (:output conn) false)
    ;(sub mchans/output-publisher :system output)
    (swap! app-state 
           update-in [:sessions session] 
           merge {:session session
                  :output output
                  :output-mult output-mult
                  :input input
                  :input-mix input-mix})
    (if-not (:room user)
      (user/sub-room user "default"))
    (swap! app-state update-in [:sessions session :connections] (fnil inc 0))
    input)) 

(defn drop-conn [{:keys [session]}]
  (log "Decrementing the conection count.")
  (swap! app-state update-in [:sessions session :connections] (fnil dec 0)))
