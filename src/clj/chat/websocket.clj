(ns chat.websocket
  (:require [chat.log :refer [log]]
            [org.httpkit.server 
             :refer [with-channel on-close on-receive send! open?]]
            [clojure.core.async :as async
             :refer [chan <! >! close! go go-loop]]
            [org.httpkit.server :refer [with-channel on-close on-receive send! open?]]
            [clojure.edn :as edn]
            ))

(defn read-data [data]
  (try 
    (clojure.edn/read-string data)
    (catch Exception e nil)))

(defn accept
  "Accepts a websocket upgrade request connection,
  adapts its communication model to core.async channels,
  and returns a setup function."
  [request control-chan]
  (let [in (chan)
        out (chan)]
    (log "New connect established.")
    (go (>! control-chan 
            {:type :opening
             :session (:session request)
             :input in
             :output out}))
    (with-channel request channel
      (go-loop 
        []
        (when-let [d (<! out)]
          (log "Received outpat" d)
          (send! channel (pr-str d))
          (recur)))
      (on-receive 
        channel 
        (fn [data] 
          (when-let [d (read-data data)] 
            (go (log "Writing to master control.") 
                (>! in {:type :data
                        :session (:session request)
                        :data d}) 
                (log "Wrote to master control.")))))
      (on-close 
        channel 
        (fn [data] 
          (go (log "Connection going away.")
              (>! in
                  {:type :closing 
                   :session (:session request)
                   :data data})
              (close! in)
              (close! out)))))))
