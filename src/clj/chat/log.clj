(ns chat.log)

(defonce datalog (atom []))

(defn log [& xs]
  (swap! datalog conj (apply vector xs)))
