(ns chat.handler
  (:require [chat.master.channels :as mchans]
            [chat.master.control :as mc]
            [chat.websocket :as ws]
            [compojure.core :refer [GET defroutes]]
            [compojure.route :refer [not-found resources]]
            [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
            [org.httpkit.server :refer [with-channel websocket? on-close on-receive send! open?]]
            [clojure.core.async :as async
             :refer [chan <! >! timeout close! go go-loop mix admix mult tap pub sub unsub alts!]]
            [clojure.edn :as edn]
            [hiccup.core :refer [html]]
            [hiccup.page :refer [include-js include-css]]
            [prone.middleware :refer [wrap-exceptions]]
            [ring.middleware.reload :refer [wrap-reload]]
            [environ.core :refer [env]]
            [clojure.pprint :refer [pprint]]))

(def home-page
  (html
    [:html
     [:head
      [:meta {:charset "utf-8"}]
      [:meta {:name "viewport"
              :content "width=device-width, initial-scale=1"}]
      (include-css (if (env :dev) "css/site.css" "css/site.min.css"))]
     [:body
      [:div#app
       [:h3 "ClojureScript has not been compiled!"]
       [:p "please run "
        [:b "lein figwheel"]
        " in order to start the compiler"]]
      (include-js "js/app.js")]]))

(defonce visits (atom 9))

(defn test-handler [req]
  (html [:pre  (with-out-str (pprint req))]))
(defn chat-handler [req]
  (if (:websocket? req) 
    (ws/accept req mchans/input-channel)
    (str "You don't belong here sonny jim.")))

(defroutes routes
  (GET "/" [] home-page)
  (GET "/test" [] test-handler)
  (GET "/chat" [] chat-handler)
  (resources "/")
  (not-found "Not Found"))

(def app
  (let [handler (wrap-defaults #'routes site-defaults)]
    (if (env :dev) (-> handler wrap-exceptions wrap-reload) handler)))
