(ns chat.user
  (:require [chat.master.channels :as mchans]
            [chat.state :refer [app-state]]
            [clojure.core.async :refer [sub unsub >!!]]))


(defn all-users []
  (vals (:sessions @app-state)))

(defn all-rooms []
  (set (flatten (map :room (all-users)))))

(defn update-state [user state]
  (>!! (:output user)
       {:type :state 
        :state state}))

(defn sub-room [user room]
  (unsub mchans/output-publisher (:room user) (:output user))
  (sub mchans/output-publisher (apply str [:announcement room]) (:output user) false)
  (sub mchans/output-publisher (apply str [:message room]) (:output user) false)
  (swap! app-state assoc-in [:sessions (:session user) :room] room)
  (>!! mchans/output-channel 
       {:type :announcement
        :room room 
        :message (str (:name user) " has joined " room ".")})
  (>!! mchans/output-channel 
       {:type :state
        :state {:rooms (all-rooms)}})
  (update-state user {:room room})
  )

(defn talk [user message]
  (>!! mchans/output-channel 
      {:type :message 
       :room (:room user) 
       :message (str (:name user) ": " message)}))



(defn write-user [user message]
  (>!! (:output user)
       {:type :system 
        :message message}))
