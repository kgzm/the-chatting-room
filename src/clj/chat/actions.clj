(ns chat.actions
  (:require [chat.state :refer [app-state]]
            [chat.log :refer [log]]
            [chat.master.channels :as mchans]
            [clojure.string :as string]
            [chat.user :as user]
            [clojure.core.async :refer [sub unsub go >! >!!]]))

(defn cmd-nick [user & [[name]]]
  (if name
    (do (swap! app-state assoc-in [:sessions (:session user) :name] name)
        (user/write-user user (str "Your name is now " name ".")))
    (user/write-user user "Who are you trying to be?!")))

(defn cmd-join [user & [[room]]]
  (log "attempting to join on" user room)
  (if room
    (user/sub-room user room)))

(defn try-command [user text]
  (if (= \/ (first text))
    (let [[cmd & args] (string/split (string/join (drop 1 text)) #"\s+")
          ]
      (log "Lookign for command" cmd)
      (condp = (string/lower-case cmd)
        "nick" (cmd-nick user args)
        "join" (cmd-join user args)
        true)
      true)
    false))

(defn dispatch [{:keys [data session] :as msg}]
  (let [{:keys [output room] :as user} 
        (get-in @app-state [:sessions session])]
    (if-not (try-command user (:message data))
      (go (log "Writing output to master output on " room)
          (user/talk user (:message data))
          (log "Wrote output to master output"))
      true)))
