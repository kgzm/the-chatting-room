(ns chat.repl
  (:require [chat.server]))

(defonce server (atom nil))


(defn start-server []
  (reset! server (chat.server/-main)))

(defn stop-server []
  (@server)
  (reset! server nil))
